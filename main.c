#include <stdlib.h>
#include <stdio.h>
#include <locale.h>
#include <string.h>

// ����� ��� ������ �� ���������
#define STR_DEF_LEN 20

// ���� ������
#define SUCCESS 0
#define NUMBER_ERR_CODE 1
#define OPERATION_ERR_CODE 2
#define DIVISION_ZERO_CODE 3

// ��������� �� ������� �����
#define NUMBER_ERR "����� ����� �������� ������"
#define OPERATION_ERR "�������� ���� ��������"

#define FALSE 0
#define TRUE 1

typedef unsigned long long ll;

/*
 * ��������� ������ �� ��������� ��������:
 * 1) ������� 0 - 9
 * 2) ����������� �� ����� 1 �����
 *
 * ���������� TRUE ���� ������ �������
 * */
int validStr(char *str) {
	ll len = strlen(str);
	int dotFlag = FALSE;

	for (ll i = 0; i < len; i++) {
		if (str[i] != '.' && !(str[i] - '0' <= 9 && str[i] - '0' >= 0)) {
			return FALSE;
		}

		if (str[i] == '.') {
			if (!dotFlag) {
				dotFlag = TRUE;
			} else {
				return FALSE;
			}
		}
	}

	return TRUE;
}

/*
 * ������� �������� ������ � �����, � ��������� ������
 * str - ������ ��� ��������
 * output - ���������� � ������� ������� �������� ���������
 *
 * ���������� FALSE ���� ������� ������ �� �������
 * */
int strToDouble(char *str, double *output) {
	*output = strtod(str, NULL);
	return !validStr(str);
}

/*
 * ������� ���������� �������� �������, ���� first operation second = result
 * �������� 13 + 4 = 17
 *
 * first - ��������� �� ������ �������
 * second - ��������� �� ������ �������
 * result - ��������� �� ����������, � ������� ����� ������� ���������
 * � ������ ������ ������ ���������� �� ������������
 * operation - ��������� �� ������������ ��������
 * */
int calculate(const double *first, const double *second, double *result, const char *operation) {
	switch (*operation) {
		case '+':
			*result = *first + *second;
			break;
		case '-':
			*result = *first - *second;
			break;
		case '*':
			*result = *first * *second;
			break;
		case '/':
			if (*second == 0.0) {
				return DIVISION_ZERO_CODE;
			}
			*result = *first / *second;
			break;
		default:
			return OPERATION_ERR_CODE;
	}

	return SUCCESS;
}

/*
 * ����� ������ �����
 * */
void printInpErr(int type) {
	printf(type == NUMBER_ERR_CODE ? NUMBER_ERR : OPERATION_ERR);
}


int main() {
	// ������� ������� �����������
	setlocale(LC_CTYPE, "Russian");

	// ���������� ��� ������������ ����������
	double first, second, res;

	char *firstStr = (char *) calloc(STR_DEF_LEN, sizeof(char));
	char *secondStr = (char *) calloc(STR_DEF_LEN, sizeof(char));
	// ������������ ��������
	char operation;


	printf("������� ������ �����:\n>");
	scanf("%s", firstStr);

	// ��������� ������ �����
	if (strToDouble(firstStr, &first)) {
		printInpErr(NUMBER_ERR_CODE);
		// ������������ ������ � ���������� ���������
		free(firstStr);
		free(secondStr);
		return -1;
	}
	free(firstStr);

	printf("������� ����:\n>");
	getchar();
	scanf("%c", &operation);

	printf("������� ����� �����:\n>");
	scanf("%s", secondStr);

	// ��������� ������ �����
	if (strToDouble(secondStr, &second)) {
		printInpErr(NUMBER_ERR_CODE);
		// ������������ ������ � ���������� ���������
		free(secondStr);
		return -1;
	}
	free(secondStr);

	// ��������� ������� ����������
	int status = calculate(&first, &second, &res, &operation);
	switch (status) {
		case SUCCESS:
			printf("���������:\n%f %c %f = %f", first, operation, second, res);
			break;
		case DIVISION_ZERO_CODE:
			printf("������ �� 0, ������");
			return -1;
		case OPERATION_ERR_CODE:
			// ��������� ������� ��������� ��������
			printInpErr(OPERATION_ERR_CODE);
			return -1;
		default:
			// ��������� ����������� ������
			printf("��������� ����������� ������");
			return -1;
	}

	return 0;
}
